//
//  ViewController.m
//  PassWordView
//
//  Created by Mars on 2018/7/20.
//  Copyright © 2018年 AirCnC车去车来. All rights reserved.
//

#import "ViewController.h"
#import "PassWordView.h"

@interface ViewController ()

@property(nonatomic,strong) PassWordView *passView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    self.passView = [[PassWordView alloc] init];
    self.passView.frame = CGRectMake(16, 100, 250, 200/6);
    
    self.passView.textBlock = ^(NSString *str) {//返回的内容
        NSLog(@"%@",str);
    };
    [self.view addSubview:_passView];
    self.passView.showType = 5;//五种样式
    self.passView.num = 6;//框框个数
    self.passView.tintColor = [UIColor orangeColor];//主题色
    [self.passView show];
    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
